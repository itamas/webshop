@extends('adminlte::page')

@section('title', 'Backoffice')

@section('content_header')
    <h1>Shipping methods</h1>
@stop

@section('content')
    <div class="row">
	    <div class="col-xs-12">
	        <table id="dataTable" class="display">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>Status</th>
	                <th>Műveletek</th>
	            </tr>
	        </thead>
	        <tbody>
	            @foreach($shippings as $item)
	            <tr>
	                <td>{{ $item->name }}</td>
	                <td>@if($item->status == 1) Aktív @else Inaktív @endif</td>
	                <td>
	                    <a href="{{ url('backoffice/shipping-methods/delete/'.$item->id) }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>Name</th>
	                <th>Status</th>
	                <th>Műveletek</th>
	            </tr>
	        </tfoot>
	    </table>
	    </div>
	</div>
@stop