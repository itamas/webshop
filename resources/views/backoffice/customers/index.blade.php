@extends('adminlte::page')

@section('title', 'Backoffice')

@section('content_header')
    <h1>Customers</h1>
@stop

@section('content')
    <div class="row">
	    <div class="col-xs-12">
	        <table id="dataTable" class="display">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>Műveletek</th>
	            </tr>
	        </thead>
	        <tbody>
	            
	            <tr>
	                <td>1</td>
	                <td>
	                    <a href="{{ url('backoffice/product/delete/') }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></a>
	                    <a href="{{ url('backoffice/product/edit/') }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></a>
	                </td>
	            </tr>
	            
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>#</th>
	                <th>Műveletek</th>
	            </tr>
	        </tfoot>
	    </table>
	    </div>
	</div>
@stop