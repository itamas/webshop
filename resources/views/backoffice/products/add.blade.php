@extends('adminlte::page')

@section('title', 'Backoffice')

@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Add new product</h3>
      </div>
      <!-- /.box-header -->
      <form action="{{ url('backoffice/product/add') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="box-body">
          <div class="form-group">
            <label>Név *</label>
            <input type="text" name="name" class="form-control" placeholder="Termék neve ..." value="{{ old('name') }}">
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-xs-4">
                <label>Price *</label>
                <input type="number" name="price" class="form-control" placeholder=".col-xs-4" value="{{ old('price') }}">
              </div>
              <div class="col-xs-4">
                <label>SKU *</label>
                <input type="text" name="sku" class="form-control" placeholder=".col-xs-4" value="{{ old('sku') }}">
              </div>
              <div class="col-xs-4">
                <label>Stock *</label>
                <input type="number" name="stock" class="form-control" placeholder=".col-xs-4" value="{{ old('stock') }}">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label>Image</label>
            <input name="image" type="file">

            <p class="help-block">Elfogadott formátumok: jpg, jpeg, png.</p>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-xs-6">
                <label>Category *</label>
                <select name="category_id" class="form-control">
                  <option value="">Choose one...</option>
                  @foreach($categories as $category)
                  <option value="{{ $category->id }}" @if(old('category_id') == $category->id) selected @endif>{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-xs-6">
                <label>Manufacturer *</label>
                <select name="manufacturer_id" class="form-control">
                  <option value="">Choose one...</option>
                  @foreach($manufacturers as $manufacturer)
                  <option value="{{ $manufacturer->id }}" @if(old('manufacturer_id') == $manufacturer->id) selected @endif>{{ $manufacturer->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <!-- textarea -->
          <div class="form-group">
            <label>Description *</label>
            <textarea name="description" class="form-control" rows="3">{{ old('description') }}</textarea>
          </div>

      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-info pull-right">Save</button>
      </div>
      <!-- /.box-body -->
      </form>
    </div>
  </div>
</div>
@stop