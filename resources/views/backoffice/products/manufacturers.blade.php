@extends('adminlte::page')

@section('title', 'Backoffice')

@section('content_header')
    <h1>Product manufacturers</h1>
@stop

@section('content')
    <div class="row">
	    <div class="col-xs-12">
	        <table id="dataTable" class="display">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>Name</th>
	                <th>Slug</th>
	                <th>Műveletek</th>
	            </tr>
	        </thead>
	        <tbody>
	            @foreach($manufacturers as $item)
	            <tr>
	                <td>{{ $item->id }}</td>
	                <td>{{ $item->name }}</td>
	                <td>{{ $item->slug }}</td>
	                <td>
	                    <a href="{{ url('backoffice/manufacturer/delete/'.$item->id) }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>#</th>
	                <th>Name</th>
	                <th>Slug</th>
	                <th>Műveletek</th>
	            </tr>
	        </tfoot>
	    </table>
	    </div>
	</div>
@stop