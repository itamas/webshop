@extends('adminlte::page')

@section('title', 'Backoffice')

@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
    <div class="row">
	    <div class="col-xs-12">
	        <table id="dataTable" class="display">
	        <thead>
	            <tr>
	                <th>#</th>
	                <th>Name</th>
	                <th>SKU</th>
	                <th>Slug</th>
	                <th>Price</th>
	                <th>Stock</th>
	                <th>Műveletek</th>
	            </tr>
	        </thead>
	        <tbody>
	            @foreach($products as $item)
	            <tr>
	                <td>{{ $item->id }}</td>
	                <td>{{ $item->name }}</td>
	                <td>{{ $item->sku }}</td>
	                <td>{{ $item->slug }}</td>
	                <td>{{ $item->price }}</td>
	                <td>@if($item->stock == 0) Nincs készleten @else {{ $item->stock }} @endif</td>
	                <td>
	                    <a href="{{ url('backoffice/product/delete/') }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></a>
	                    <a href="{{ url('backoffice/product/edit/'.$item->id) }}"><button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>#</th>
					<th>Name</th>
					<th>SKU</th>
	                <th>Slug</th>
	                <th>Price</th>
	                <th>Stock</th>
	                <th>Műveletek</th>
	            </tr>
	        </tfoot>
	    </table>
	    </div>
	</div>
@stop