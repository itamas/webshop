@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
						<div class="row">
							<div class="col-xs-6">
								<h5><span class="glyphicon glyphicon-shopping-cart"></span> Kosaram</h5>
							</div>
							<div class="col-xs-6">
								<button type="button" class="btn btn-primary btn-sm btn-block">
									<span class="glyphicon glyphicon-share-alt"></span> Vásárlás folytatása
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					@foreach($cart as $item)
					<div class="row">
						<div class="col-xs-2"><img class="img-responsive" src="http://placehold.it/100x70">
						</div>
						<div class="col-xs-4">
							<h4 class="product-name"><strong>{{ $item->name }}</strong></h4>
						</div>
						<div class="col-xs-6">
							<div class="col-xs-6 text-right">
								<h5><strong>{{ number_format($item->price, 0, ' ', ' ') }} Ft.-</strong></h5>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control input-sm" value="{{ $item->qty }}">
							</div>
							<div class="col-xs-2">
								<a href="{{ url('kosar/remove/'.$item->rowId) }}"><button type="button" class="btn btn-link btn-xs">
									<span class="glyphicon glyphicon-trash"> </span>
								</button></a>
							</div>
						</div>
					</div>
					<hr>
					@endforeach
				</div>
				<div class="panel-footer">
					<div class="row text-center">
						<div class="col-xs-9">
							<h4 class="text-right">Összesen: <strong>{{ Cart::subtotal(0, ' ', ' ') }} Ft.-</strong></h4>
						</div>
						<div class="col-xs-3">
							<button type="button" class="btn btn-success btn-block">
								Vásárlás
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop