<!doctype html>
<html>
<head>
    @include('components.head')
</head>
<body>
<div class="container">

    <header class="row">
        @include('components.header')
    </header>

    <div id="main" class="row">

            @yield('content')

    </div>

    <footer class="row">
        @include('components.footer')
    </footer>

</div>
</body>
</html>