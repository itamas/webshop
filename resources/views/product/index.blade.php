@extends('layouts.app')
@section('content')
<div class="container">
    <div class="well well-sm">
        <strong>Kategória</strong>
    </div>
    <div class="col-sm-3 col-md-3 sidebar">
        <div class="mini-submenu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </div>
        <div class="list-group">
            <span href="#" class="list-group-item active">
                Kategóriák
                <span class="pull-right" id="slide-submenu">
                    <i class="fa fa-times"></i>
                </span>
            </span>
            @foreach($category as $cat)
            <a href="{{ url('termekek/kategoria/'.$cat->slug) }}" class="list-group-item @if(Request::segment(3) == $cat->slug) active @endif">{{ $cat->name }}</a>
            @endforeach
        </div>        
    </div>
    <div id="products" class="row list-group col-sm-8">
      @foreach($products as $product)
        <div class="item col-sm-4 col-md-4">
            <div class="thumbnail">
                <img class="group list-group-image" src="{{ $product->image }}" height="100" width="100" alt="" />
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">{{ $product->name }}</h4>
                    <p class="group inner list-group-item-text">
                    Gyártó: {{ $product->manufacturer->name }} <br>
                    Kategória: {{ $product->category->name }} <br>
                    Készletinformáció: @if($product->stock > 0) Van készleten @else Nincs készleten @endif
                    </p>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="lead">{{ $product->price }} Ft.-</p>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right"><a class="btn btn-success" href="{{ url('kosar/add/'.$product->id) }}">Kosárba</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      @endforeach
    </div>
</div>
@stop