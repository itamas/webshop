<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    /**
     * Easier way to generate slug
     */
    use Sluggable;
    
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'products';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['category_id', 'manufacturer_id', 'name', 'sku', 'image', 'slug', 'description', 'price', 'stock'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['name', 'id'],
                'separator' => '-'
            ]
        ];
    }

    /**
     * Add productcategory relationship. 1to1
     */
    public function category()
    {
        return $this->hasOne('App\ProductCategory', 'id', 'category_id');
    }

    /**
     * Add productmanufacturer relationship. 1to1
     */
    public function manufacturer()
    {
        return $this->hasOne('App\ProductManufacturer', 'id', 'manufacturer_id');
    }
}
