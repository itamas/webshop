<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductCategory extends Model
{
    /**
     * Easier way to generate slug
     */
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'product_categories';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['name', 'slug'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Add productcategory relationship. 1to1
     */
    public function product()
    {
        return $this->belongsTo('App\Product', 'id', 'category_id');
    }

}
