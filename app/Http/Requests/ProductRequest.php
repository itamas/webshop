<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'category_id' => 'required',
            'manufacturer_id' => 'required',
            'price' => 'required',
            'stock' => 'required|min:0|max:200',
            'description' => 'required',
            'image' => 'max:4096',
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
