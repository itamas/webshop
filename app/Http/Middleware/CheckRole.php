<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class CheckRole
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if (! $request->user()->hasAnyRole($roles)) {
            return redirect('/');
        }

        return $next($request);
    }

}