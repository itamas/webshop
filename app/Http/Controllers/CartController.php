<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Cart;
use App\Product;
use Redirect;

class CartController extends Controller {

	public function __construct() {
        //$this->middleware('auth');
    }

    public function cart() {
        $cart = Cart::content();
        return view('pages.cart', array('cart' => $cart));
    }

    public function addToCart($id) {
    	$product = Product::where('id', $id)->first();
    	Cart::add($product->id, $product->name, 1, $product->price);
    	return Redirect::to('kosar');
    }

    public function removeFromCart($rowId) {
    	Cart::remove($rowId);
    	return Redirect::to('kosar');
    }

}