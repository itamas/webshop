<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use App\ProductCategory;
use App\ProductManufacturer;
use App\Payment;
use App\Shipping;

class BackofficeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('backoffice.index');
    }

    /**
     * Orders
     */
    public function orderList() {
        return view('backoffice.orders.index');
    }

    /**
     * Products
     */
    public function productList() {
        $products = Product::all();
        return view('backoffice.products.index', array('products' => $products));
    }

    public function productAdd() {
        $categories = ProductCategory::all();
        $manufacturers = ProductManufacturer::all();
        return view('backoffice.products.add', array('categories' => $categories, 'manufacturers' => $manufacturers));
    }

    public function categoryList() {
        $categories = ProductCategory::all();
        return view('backoffice.products.categories', array('categories' => $categories));
    }

    public function manufacturerList() {
        $manufacturers = ProductManufacturer::all();
        return view('backoffice.products.manufacturers', array('manufacturers' => $manufacturers));
    }

    /**
     * Customers
     */
    public function customerList() {
        return view('backoffice.customers.index');
    }

    /**
     * Methods
     */
    public function paymentList() {
        $payments = Payment::all();
        return view('backoffice.payment.index', array('payments' => $payments));
    }

    public function shippingList() {
        $shippings = Shipping::all();
        return view('backoffice.shipping.index', array('shippings' => $shippings));
    }
}
