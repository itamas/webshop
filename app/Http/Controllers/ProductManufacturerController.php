<?php

namespace App\Http\Controllers;

use App\ProductManufacturer;
use Illuminate\Http\Request;

class ProductManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductManufacturer  $productManufacturer
     * @return \Illuminate\Http\Response
     */
    public function show(ProductManufacturer $productManufacturer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductManufacturer  $productManufacturer
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductManufacturer $productManufacturer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductManufacturer  $productManufacturer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductManufacturer $productManufacturer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductManufacturer  $productManufacturer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $manufacturer = ProductManufacturer::findOrFail($id);
        $manufacturer->delete();
        //Alert::success('Sikeres törlés', 'Sikeresen törölted!');
        return redirect('backoffice/products/manufacturers');
    }
}
