<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductManufacturer;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Redirect;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $validator = Validator::make($request->all(), $request->rules(), $request->messages());

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $product = Product::create(['category_id' => $request->input('category_id'),
                                    'manufacturer_id' => $request->input('manufacturer_id'),
                                    'name' => $request->input('name'),
                                    'sku' => $request->input('sku'),
                                    'image' => $request->input('image'),
                                    'description' => $request->input('description'),
                                    'price' => $request->input('price'),
                                    'stock' => $request->input('stock'),
                                    ]);

            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->file('image')->extension();
                $request->file('image')->move(public_path('uploads/products'), $imageName);
                $product->image = $imageName;
            }

            $product->save();

        }

        return Redirect::to('backoffice/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = ProductCategory::all();
        $manufacturers = ProductManufacturer::all();
        return view('backoffice.products.edit', array('categories' => $categories, 'manufacturers' => $manufacturers, 'item' => $product));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $validator = Validator::make($request->all(), $request->rules(), $request->messages());

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $product = Product::where('id', $id)->update(['category_id' => $request->input('category_id'),
                                    'manufacturer_id' => $request->input('manufacturer_id'),
                                    'name' => $request->input('name'),
                                    'sku' => $request->input('sku'),
                                    'image' => $request->input('image'),
                                    'description' => $request->input('description'),
                                    'price' => $request->input('price'),
                                    'stock' => $request->input('stock'),
                                    ]);

        }

        return Redirect::to('backoffice/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        //Alert::success('Sikeres törlés', 'Sikeresen törölted!');
        return redirect('backoffice/products');
    }
}
