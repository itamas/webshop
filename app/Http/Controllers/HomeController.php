<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use App\ProductCategory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index() {
        return view('pages.index');
    }

    public function productsIndex() {
        $products = Product::with(['category', 'manufacturer'])->get();
        $category = ProductCategory::all();  
        return view('product.index', array('products' => $products, 'category' => $category));
    }

    public function productCategory($slug) {
        if($slug == "all") {
            $products = Product::with(['category', 'manufacturer'])->get();
        } else {
            $cat = ProductCategory::where('slug', $slug)->first();
            $products = Product::with(['category', 'manufacturer'])->where('category_id', $cat->id)->get();
        } 
        $category = ProductCategory::all();
        return view('product.index', array('products' => $products, 'category' => $category));
    }

}
