<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 0; $i < 100; $i++) {
    		$random_string = str_random(10);
	        DB::table('products')->insert([
	        	'category_id' => rand(1,4),
	        	'manufacturer_id' => rand(1,10),
	            'name' => "Szuper termék:".$random_string,
	            'image' => "https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg",
	            'slug' => "szuper-termek-".$random_string,
	            'description' => "Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)",
	            'price' => rand(5000,130000),
	            'stock' => rand(0,4)
	        ]);
	    }
    }
}
