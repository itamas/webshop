-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2018. Máj 21. 09:14
-- Kiszolgáló verziója: 10.2.9-MariaDB
-- PHP verzió: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `webshop`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2018_03_19_200022_create_products_table', 1),
(7, '2018_03_21_143650_create_product_categories_table', 2),
(8, '2018_03_21_144656_create_product_manufacturers_table', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Bankkártyás fizetés', 1, '2018-04-01 22:00:00', '0000-00-00 00:00:00'),
(3, 'Készpénz', 1, '2018-04-01 22:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `manufacturer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `products`
--

INSERT INTO `products` (`id`, `category_id`, `manufacturer_id`, `name`, `sku`, `image`, `slug`, `description`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'Szuper termék', '1', NULL, 'szuper-termek-am7eUXhmYp', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 94434, 400, '2018-04-25 18:52:22', '2018-04-25 16:44:28'),
(2, 4, 8, 'Szuper termék:COLGqbRjgy', '2', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-COLGqbRjgy', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 121337, 1, '2018-04-25 18:52:22', NULL),
(3, 1, 1, 'Szuper termék:FbBbadvPkb', '3', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-FbBbadvPkb', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 127275, 0, '2018-04-25 18:52:22', NULL),
(4, 4, 10, 'Szuper termék:mpid8eqXbQ', '4', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-mpid8eqXbQ', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 19724, 0, '2018-04-25 18:52:22', NULL),
(5, 3, 1, 'Szuper termék:9949xwl9r1', '5', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-9949xwl9r1', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 41693, 0, '2018-04-25 18:52:22', NULL),
(6, 3, 2, 'Szuper termék:79nan78BS6', '6', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-79nan78BS6', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 35082, 2, '2018-04-25 18:52:22', NULL),
(7, 3, 5, 'Szuper termék:bJiiyWP48e', '7', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-bJiiyWP48e', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 12865, 1, '2018-04-25 18:52:22', NULL),
(8, 2, 4, 'Szuper termék:Y6uXrGhE7A', '8', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-Y6uXrGhE7A', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 39358, 4, '2018-04-25 18:52:22', NULL),
(9, 3, 5, 'Szuper termék:aqH5rQenfA', '9', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-aqH5rQenfA', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 10715, 3, '2018-04-25 18:52:22', NULL),
(10, 3, 3, 'Szuper termék:bdOJ6cl7nF', '10', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-bdOJ6cl7nF', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 42681, 0, '2018-04-25 18:52:22', NULL),
(11, 2, 4, 'Szuper termék:5CUczO0KvN', '11', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-5CUczO0KvN', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 15462, 3, '2018-04-25 18:52:22', NULL),
(12, 4, 4, 'Szuper termék:iExOOkU6rY', '12', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-iExOOkU6rY', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 100080, 3, '2018-04-25 18:52:22', NULL),
(13, 1, 5, 'Szuper termék:35sZcVy56f', '13', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-35sZcVy56f', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 78908, 3, '2018-04-25 18:52:22', NULL),
(14, 4, 1, 'Szuper termék:sZyBhSYhhZ', '14', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-sZyBhSYhhZ', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 72859, 4, '2018-04-25 18:52:22', NULL),
(15, 4, 8, 'Szuper termék:p9MXKiqGwF', '15', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-p9MXKiqGwF', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 51631, 2, '2018-04-25 18:52:22', NULL),
(16, 3, 9, 'Szuper termék:AatcHRsRW4', '16', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-AatcHRsRW4', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 22481, 2, '2018-04-25 18:52:22', NULL),
(17, 1, 1, 'Szuper termék:Db661lIEte', '17', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-Db661lIEte', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 60531, 1, '2018-04-25 18:52:22', NULL),
(18, 3, 7, 'Szuper termék:kDP0m31Ior', '18', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-kDP0m31Ior', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 107390, 1, '2018-04-25 18:52:22', NULL),
(19, 2, 5, 'Szuper termék:YkvKiiQ8cF', '19', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-YkvKiiQ8cF', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 96696, 1, '2018-04-25 18:52:22', NULL),
(20, 2, 2, 'Szuper termék:F7TXlZ362x', '20', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-F7TXlZ362x', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 34138, 1, '2018-04-25 18:52:22', NULL),
(21, 1, 8, 'Szuper termék:NEwEQzdSik', '21', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-NEwEQzdSik', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 52900, 4, '2018-04-25 18:52:22', NULL),
(22, 2, 10, 'Szuper termék:W3UrsB1Xeb', '22', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-W3UrsB1Xeb', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 56978, 0, '2018-04-25 18:52:22', NULL),
(23, 1, 3, 'Szuper termék:Noz5W68Q8m', '23', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-Noz5W68Q8m', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 22439, 3, '2018-04-25 18:52:22', NULL),
(24, 2, 5, 'Szuper termék:XvXQn9qj4v', '24', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-XvXQn9qj4v', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 100741, 1, '2018-04-25 18:52:22', NULL),
(25, 1, 2, 'Szuper termék:WbdInifAEp', '25', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-WbdInifAEp', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 92479, 4, '2018-04-25 18:52:22', NULL),
(26, 4, 10, 'Szuper termék:QAeEWVV0Xx', '26', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-QAeEWVV0Xx', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 121970, 0, '2018-04-25 18:52:22', NULL),
(27, 3, 3, 'Szuper termék:f5AMqlwkCU', '27', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-f5AMqlwkCU', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 66827, 0, '2018-04-25 18:52:22', NULL),
(28, 2, 4, 'Szuper termék:fZuISAae4k', '28', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-fZuISAae4k', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 50130, 1, '2018-04-25 18:52:22', NULL),
(29, 4, 4, 'Szuper termék:S4ft2PT06O', '29', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-S4ft2PT06O', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 27081, 1, '2018-04-25 18:52:22', NULL),
(30, 2, 3, 'Szuper termék:PNgQfDgR8C', '30', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-PNgQfDgR8C', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 11949, 1, '2018-04-25 18:52:22', NULL),
(31, 2, 6, 'Szuper termék:qBKyj2fPi4', '31', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-qBKyj2fPi4', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 125078, 0, '2018-04-25 18:52:22', NULL),
(32, 1, 1, 'Szuper termék:dtCpcMBd9k', '32', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-dtCpcMBd9k', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 114617, 2, '2018-04-25 18:52:22', NULL),
(33, 4, 10, 'Szuper termék:9V5tUYwkLd', '33', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-9V5tUYwkLd', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 105845, 2, '2018-04-25 18:52:22', NULL),
(34, 3, 1, 'Szuper termék:KqSZQDuBiM', '34', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-KqSZQDuBiM', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 79877, 4, '2018-04-25 18:52:22', NULL),
(35, 3, 1, 'Szuper termék:B2YZ7QYLvk', '35', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-B2YZ7QYLvk', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 6139, 0, '2018-04-25 18:52:22', NULL),
(36, 4, 6, 'Szuper termék:TxAqIbUF0G', '36', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-TxAqIbUF0G', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 103397, 0, '2018-04-25 18:52:22', NULL),
(37, 2, 1, 'Szuper termék:msSzfG2gUS', '37', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-msSzfG2gUS', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 81354, 2, '2018-04-25 18:52:22', NULL),
(38, 2, 7, 'Szuper termék:95vU5dkHRF', '38', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-95vU5dkHRF', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 92158, 1, '2018-04-25 18:52:22', NULL),
(39, 2, 10, 'Szuper termék:3PyjaY0x0w', '39', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-3PyjaY0x0w', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 13162, 3, '2018-04-25 18:52:22', NULL),
(40, 1, 8, 'Szuper termék:kMJztL4DlM', '40', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-kMJztL4DlM', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 12269, 4, '2018-04-25 18:52:22', NULL),
(41, 2, 6, 'Szuper termék:Ace4DFYWuO', '41', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-Ace4DFYWuO', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 49320, 0, '2018-04-25 18:52:22', NULL),
(42, 2, 6, 'Szuper termék:W7vFkk3wBh', '42', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-W7vFkk3wBh', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 66239, 1, '2018-04-25 18:52:22', NULL),
(43, 2, 2, 'Szuper termék:8lzDlSHStH', '43', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-8lzDlSHStH', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 68125, 2, '2018-04-25 18:52:22', NULL),
(44, 2, 10, 'Szuper termék:0c1HwBj2dC', '44', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-0c1HwBj2dC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 63718, 0, '2018-04-25 18:52:22', NULL),
(45, 2, 10, 'Szuper termék:n51BQ9Spdi', '45', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-n51BQ9Spdi', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 28309, 0, '2018-04-25 18:52:22', NULL),
(46, 1, 2, 'Szuper termék:auLXGAuQAw', '46', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-auLXGAuQAw', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 81275, 4, '2018-04-25 18:52:22', NULL),
(47, 4, 2, 'Szuper termék:ELQcMeePWS', '47', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ELQcMeePWS', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 75141, 0, '2018-04-25 18:52:22', NULL),
(48, 4, 2, 'Szuper termék:DpAZcVyvCa', '48', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-DpAZcVyvCa', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 62668, 2, '2018-04-25 18:52:22', NULL),
(49, 4, 2, 'Szuper termék:zaJYfOhOuj', '49', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-zaJYfOhOuj', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 79912, 2, '2018-04-25 18:52:22', NULL),
(50, 1, 4, 'Szuper termék:TArxP0IG4p', '50', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-TArxP0IG4p', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 73926, 1, '2018-04-25 18:52:22', NULL),
(51, 4, 4, 'Szuper termék:ut8nQFlkMz', '51', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ut8nQFlkMz', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 113972, 4, '2018-04-25 18:52:22', NULL),
(52, 4, 2, 'Szuper termék:fCMbCuVD17', '52', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-fCMbCuVD17', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 28765, 1, '2018-04-25 18:52:22', NULL),
(53, 3, 5, 'Szuper termék:2wBr7XXdo6', '53', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-2wBr7XXdo6', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 33187, 3, '2018-04-25 18:52:22', NULL),
(54, 4, 6, 'Szuper termék:m5qs1mYyiL', '54', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-m5qs1mYyiL', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 34202, 2, '2018-04-25 18:52:22', NULL),
(55, 2, 5, 'Szuper termék:IJt6fH45E7', '55', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-IJt6fH45E7', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 39862, 4, '2018-04-25 18:52:22', NULL),
(56, 4, 9, 'Szuper termék:NgWEektPfe', '56', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-NgWEektPfe', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 35662, 3, '2018-04-25 18:52:22', NULL),
(57, 4, 6, 'Szuper termék:JlZGVHkRay', '57', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-JlZGVHkRay', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 109749, 3, '2018-04-25 18:52:22', NULL),
(58, 1, 2, 'Szuper termék:GhtfrrKbAk', '58', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-GhtfrrKbAk', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 95996, 3, '2018-04-25 18:52:22', NULL),
(59, 3, 7, 'Szuper termék:Ei64jyP4LG', '59', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-Ei64jyP4LG', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 74721, 2, '2018-04-25 18:52:22', NULL),
(60, 2, 5, 'Szuper termék:brga1HfDYr', '60', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-brga1HfDYr', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 45559, 3, '2018-04-25 18:52:22', NULL),
(61, 2, 8, 'Szuper termék:WcAlb08joP', '61', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-WcAlb08joP', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 48327, 3, '2018-04-25 18:52:22', NULL),
(62, 1, 2, 'Szuper termék:uN64dCNsoE', '62', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-uN64dCNsoE', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 111898, 4, '2018-04-25 18:52:22', NULL),
(63, 1, 3, 'Szuper termék:ooqkDKW2KC', '63', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ooqkDKW2KC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 24407, 0, '2018-04-25 18:52:22', NULL),
(64, 1, 6, 'Szuper termék:miCn3rpmou', '64', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-miCn3rpmou', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 39956, 3, '2018-04-25 18:52:22', NULL),
(65, 2, 8, 'Szuper termék:VDMHFeUcCN', '65', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-VDMHFeUcCN', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 67853, 4, '2018-04-25 18:52:22', NULL),
(66, 1, 2, 'Szuper termék:YpiSSCiklL', '66', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-YpiSSCiklL', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 103873, 1, '2018-04-25 18:52:22', NULL),
(67, 2, 6, 'Szuper termék:FHwOx1PrdV', '67', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-FHwOx1PrdV', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 46823, 2, '2018-04-25 18:52:22', NULL),
(68, 2, 4, 'Szuper termék:i6nKEmU6bC', '68', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-i6nKEmU6bC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 94679, 2, '2018-04-25 18:52:22', NULL),
(69, 1, 7, 'Szuper termék:mUv3yHTcuk', '69', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-mUv3yHTcuk', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 5680, 2, '2018-04-25 18:52:22', NULL),
(70, 2, 1, 'Szuper termék:pNquiULD4D', '70', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-pNquiULD4D', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 129317, 2, '2018-04-25 18:52:22', NULL),
(71, 2, 9, 'Szuper termék:ytNLzf4Gvi', '71', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ytNLzf4Gvi', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 113998, 3, '2018-04-25 18:52:22', NULL),
(72, 4, 6, 'Szuper termék:TAaGxz5Xxx', '72', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-TAaGxz5Xxx', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 70919, 3, '2018-04-25 18:52:22', NULL),
(73, 1, 8, 'Szuper termék:ZzFH9zDsK1', '73', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ZzFH9zDsK1', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 19349, 1, '2018-04-25 18:52:22', NULL),
(74, 4, 5, 'Szuper termék:NfJYIPTP2c', '74', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-NfJYIPTP2c', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 56235, 4, '2018-04-25 18:52:22', NULL),
(75, 1, 3, 'Szuper termék:hTKT6avwzl', '75', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-hTKT6avwzl', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 96176, 2, '2018-04-25 18:52:22', NULL),
(76, 4, 4, 'Szuper termék:dJlpmMeyDb', '76', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-dJlpmMeyDb', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 77137, 0, '2018-04-25 18:52:22', NULL),
(77, 3, 7, 'Szuper termék:zBP8r764Hr', '77', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-zBP8r764Hr', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 44368, 2, '2018-04-25 18:52:22', NULL),
(78, 4, 5, 'Szuper termék:EeMHABLXEC', '78', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-EeMHABLXEC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 80091, 3, '2018-04-25 18:52:22', NULL),
(79, 4, 5, 'Szuper termék:4mmbTQgoYK', '79', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-4mmbTQgoYK', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 94951, 3, '2018-04-25 18:52:22', NULL),
(80, 3, 3, 'Szuper termék:IopZr8B2Az', '80', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-IopZr8B2Az', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 80864, 3, '2018-04-25 18:52:22', NULL),
(81, 1, 9, 'Szuper termék:TplaomuQby', '81', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-TplaomuQby', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 36471, 0, '2018-04-25 18:52:22', NULL),
(82, 1, 4, 'Szuper termék:PZ3SaOWgkh', '82', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-PZ3SaOWgkh', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 34699, 1, '2018-04-25 18:52:22', NULL),
(83, 1, 6, 'Szuper termék:wQbtjrXqUk', '83', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-wQbtjrXqUk', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 40257, 4, '2018-04-25 18:52:22', NULL),
(84, 3, 4, 'Szuper termék:KQUrdS6R7F', '84', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-KQUrdS6R7F', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 19343, 0, '2018-04-25 18:52:22', NULL),
(85, 1, 10, 'Szuper termék:IWb4gbf0p6', '85', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-IWb4gbf0p6', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 52169, 4, '2018-04-25 18:52:22', NULL),
(86, 2, 2, 'Szuper termék:4xPgpE8RwW', '86', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-4xPgpE8RwW', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 127963, 0, '2018-04-25 18:52:22', NULL),
(87, 4, 6, 'Szuper termék:58gqIXOc3e', '87', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-58gqIXOc3e', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 9835, 0, '2018-04-25 18:52:22', NULL),
(88, 2, 4, 'Szuper termék:NCQCxZfyPs', '88', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-NCQCxZfyPs', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 55615, 3, '2018-04-25 18:52:22', NULL),
(89, 1, 9, 'Szuper termék:4JtPMjudd4', '89', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-4JtPMjudd4', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 122726, 3, '2018-04-25 18:52:22', NULL),
(90, 3, 4, 'Szuper termék:VNM7oodyf2', '90', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-VNM7oodyf2', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 54326, 2, '2018-04-25 18:52:22', NULL),
(91, 1, 1, 'Szuper termék:R7s8HsB9Ny', '91', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-R7s8HsB9Ny', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 103791, 4, '2018-04-25 18:52:22', NULL),
(92, 2, 10, 'Szuper termék:szDWjNjugm', '92', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-szDWjNjugm', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 77053, 3, '2018-04-25 18:52:22', NULL),
(93, 2, 6, 'Szuper termék:JU0xXUm1XN', '93', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-JU0xXUm1XN', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 95173, 1, '2018-04-25 18:52:22', NULL),
(94, 3, 6, 'Szuper termék:rLH0VhnD5g', '94', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-rLH0VhnD5g', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 17082, 2, '2018-04-25 18:52:22', NULL),
(95, 4, 2, 'Szuper termék:L7aw3CWKjO', '95', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-L7aw3CWKjO', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 108171, 3, '2018-04-25 18:52:22', NULL),
(96, 4, 5, 'Szuper termék:wGBSGnhGtC', '96', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-wGBSGnhGtC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 69101, 1, '2018-04-25 18:52:22', NULL),
(97, 4, 6, 'Szuper termék:OiNkGict8j', '97', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-OiNkGict8j', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 117420, 0, '2018-04-25 18:52:22', NULL),
(98, 2, 3, 'Szuper termék:3qwWzqlz6V', '98', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-3qwWzqlz6V', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 76276, 4, '2018-04-25 18:52:22', NULL),
(99, 1, 1, 'Szuper termék:TcbXFx6EbC', '99', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-TcbXFx6EbC', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 93622, 1, '2018-04-25 18:52:22', NULL),
(100, 3, 1, 'Szuper termék:ikDR5A3tXD', '100', 'https://www.aqua.hu/files/pix-image/299/asus-geforce-gtx-1080-ti-11gb-rog-strix-videokartya-rog-strix-gtx1080ti-o11g-gaming-kupon-467519.jpg', 'szuper-termek-ikDR5A3tXD', 'Hosszú leírás, amit most nyílván nem fogok megírni, mert akkor sose lenne leadva a projekt. (meg amúgyis minek regényeket írni?)', 52727, 1, '2018-04-25 18:52:22', NULL),
(101, 1, 1, 'dsadsa', '2134tzhnbv', '30415074_1863145270383428_700492425324114667_n.jpg', 'dsadsa', 'sdsfghhbgvfcd', 213456, 23421, '2018-04-18 15:06:35', '2018-04-18 15:06:35'),
(102, 1, 1, 'dasdasdsadsa', '213rtvcd', '30415074_1863145270383428_700492425324114667_n.jpg', 'dasdasdsadsa', 'adsfsdgfhnbgfvsdcs', 13245, 21, '2018-04-18 15:07:10', '2018-04-18 15:07:10'),
(103, 1, 3, 'adfsdgf', '1324354', '30415074_1863145270383428_700492425324114667_n.jpg', 'adfsdgf', 'dfsdgfsdadv', 3245, 312243, '2018-04-18 15:08:07', '2018-04-18 15:08:07'),
(104, 1, 1, '13245t4r3', 'dfsgf', '1524071653.jpeg', '13245t4r3', 'dasfdfgghnbgfd', 13243, 312, '2018-04-18 15:14:13', '2018-04-18 15:14:13');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Alaplap', 'alaplap', '2018-04-25 18:52:35', NULL),
(2, 'Processzor', 'processzor', '2018-04-25 18:52:35', NULL),
(3, 'Memória', 'memoria', '2018-04-25 18:52:35', NULL),
(4, 'Videókártya', 'videokartya', '2018-04-25 18:52:35', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `product_manufacturers`
--

CREATE TABLE `product_manufacturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `product_manufacturers`
--

INSERT INTO `product_manufacturers` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'INTEL', 'intel', '2018-04-25 18:52:05', NULL),
(2, 'AMD', 'amd', '2018-04-25 18:52:05', NULL),
(3, 'NVIDIA', 'nvidia', '2018-04-25 18:52:05', NULL),
(4, 'Logitech', 'logitech', '2018-04-25 18:52:05', NULL),
(5, 'ASUS', 'asus', '2018-04-25 18:52:05', NULL),
(6, 'Apple', 'apple', '2018-04-25 18:52:05', NULL),
(7, 'Zotac', 'Zotac', '2018-04-25 18:52:05', NULL),
(8, 'HP', 'hp', '2018-04-25 18:52:05', NULL),
(9, 'Dell', 'dell', '2018-04-25 18:52:05', NULL),
(10, 'LG', 'lg', '2018-04-25 18:52:05', NULL),
(11, 'Samsung', 'samsung', '2018-04-25 18:52:05', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-04-10 22:00:00', '0000-00-00 00:00:00'),
(2, 'customer', 'Customer', '2018-04-10 22:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `role_user`
--

CREATE TABLE `role_user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `shipping_methods`
--

CREATE TABLE `shipping_methods` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Személyes átvétel', 1, '2018-04-01 22:00:00', '0000-00-00 00:00:00'),
(2, 'GLS futárszolgálat', 1, '2018-04-01 22:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tomi', 'deagle08@gmail.com', '$2y$10$hxKuaOSspibNI4QygJuZAOD2f/1ON7dawFga6DKPFweK2Gdhgp43e', 'KjKNkQpnVkRdoD4yIynG2wBmkjDpzY7zBa4ksmC5Vs3GMZKMXdiDTDzQXyEr', '2018-04-18 09:17:37', '2018-04-18 09:17:37'),
(2, 'Marci', 'gyopos18@gmail.com', '$2y$10$3byXE1EBol0.0LAqSNFZju2wU8X.o9lYfAkH7H4Of0BXKIbFMXQfm', NULL, '2018-05-21 07:13:33', '2018-05-21 07:13:33');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- A tábla indexei `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`);

--
-- A tábla indexei `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `product_manufacturers`
--
ALTER TABLE `product_manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_ibfk_1` (`user_id`),
  ADD KEY `role_user_ibfk_2` (`role_id`);

--
-- A tábla indexei `shipping_methods`
--
ALTER TABLE `shipping_methods`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT a táblához `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `product_manufacturers`
--
ALTER TABLE `product_manufacturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `shipping_methods`
--
ALTER TABLE `shipping_methods`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `product_manufacturers` (`id`);

--
-- Megkötések a táblához `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
