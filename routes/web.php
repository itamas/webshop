<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::prefix('kosar')->group(function () {
	Route::get('/', 'CartController@cart');
	Route::get('/add/{id}', 'CartController@addToCart');
	Route::get('/remove/{rowId}', 'CartController@removeFromCart');
});
Auth::routes();

Route::prefix('termekek')->group(function () {
    Route::get('/', 'HomeController@productsIndex');
    Route::get('/kategoria/{slug}', 'HomeController@productCategory');
});
Route::prefix('backoffice')->group(function () {
	Route::get('/', 'BackofficeController@index');

	Route::get('/products', 'BackofficeController@productList');
	Route::get('/product/add', 'BackofficeController@productAdd');
	Route::post('/product/add', 'ProductController@store');
	Route::get('/product/edit/{id}', 'ProductController@edit');
	Route::post('/product/edit/{id}', 'ProductController@update');
	Route::get('/products/categories', 'BackofficeController@categoryList');
	Route::get('/products/manufacturers', 'BackofficeController@manufacturerList');
	Route::get('/products/manufacturer/delete/{id}', 'ProductManufacturerController@destroy');
	Route::get('/products/category/delete/{id}', 'ProductCategoryController@destroy');

	Route::get('/orders', 'BackofficeController@orderList');
	Route::get('/shipping-methods', 'BackofficeController@shippingList');
	Route::get('/shipping-methods/delete/{id}', 'ShippingController@destroy');
	Route::get('/payment-methods', 'BackofficeController@paymentList');
	Route::get('/payment-methods/delete/{id}', 'PaymentController@destroy');

	Route::get('/customers', 'BackofficeController@customerList');
});
