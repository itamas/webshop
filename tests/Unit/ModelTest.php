<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Product;
use App\Payment;

class ModelTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        //User
        $user = new User(['name'=>'Test User']);
        $this->assertEquals('Test User', $user->name);

        /*Product
        $products = Product::get();
        $this->assertFalse($products->count() == 105);
        $this->assertTrue($products->count() == 104);

        $payment = Payment::create(['name' => 'Teszt fizetés', 'status' => 0]);
        $payments = Payment::get();
        $this->assertTrue($payments->count() == 3);
        $paymentQuery = Payment::where('name', 'Teszt fizetés')->first();
        $paymentQuery->delete();
        $payments = Payment::get();
        $this->assertFalse($payments->count() == 3);*/

    }

}
